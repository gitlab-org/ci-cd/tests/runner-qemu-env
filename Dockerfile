ARG UBUNTU_VERSION
FROM ubuntu:${UBUNTU_VERSION}

RUN ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
RUN apt update -y
RUN apt install -y git git-lfs curl file qemu-user qemu-user-static gcc-aarch64-linux-gnu binutils-aarch64-linux-gnu binutils-aarch64-linux-gnu-dbg gcc-s390x-linux-gnu binutils-s390x-linux-gnu binutils-s390x-linux-gnu-dbg gcc-powerpc64le-linux-gnu binutils-powerpc64le-linux-gnu binutils-powerpc64le-linux-gnu-dbg build-essential
